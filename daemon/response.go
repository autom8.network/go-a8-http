package daemon

import "net/http"

const (
	// OK means all is 👍
	OK = http.StatusOK
	// NotFound means a request resulted in something not being found
	NotFound = http.StatusNotFound
)

// Response response encapsulates the status code (success, fail, etc) as well as the data retured
type Response struct {
	Status int
	Data   []byte
}
