package client

import (
	"bytes"
	"fmt"
	"github.com/golang/protobuf/proto"
	a8Util "gitlab.com/autom8.network/go-a8-util"
	a8crypto "gitlab.com/autom8.network/go-a8-util/crypto"
	pb "gitlab.com/autom8.network/go-a8-util/proto"
	"io/ioutil"
	"net/http"
)

var client = &http.Client{}

const errorMsg = "Failed to invoke a8 command. Is the a8 daemon running?"

type daemonSenderError struct{}

// Config holds the instance of DaemonConfig which should be created via the NewDaemonConfig
var Config *DaemonConfig

func (d *daemonSenderError) Error() string {
	return errorMsg
}

func daemonMessage(request *pb.A8Request) (*pb.A8Response, error) {
	a8Response := &pb.A8Response{}

	var fullURL = fmt.Sprintf("%smessage", Config.URL)

	//signs the request with the clients private key (if there is one)
	if err := signRequest(request); err != nil {
		return nil, err
	}

	dataRaw, err := proto.Marshal(request)
	if err != nil {
		return nil, err
	}

	var buf = bytes.NewBuffer(dataRaw)

	req, err := http.NewRequest(http.MethodPost, fullURL, buf)
	if err != nil {
		return nil, err
	}

	req.Close = true
	resp, err := client.Do(req)
	if err != nil {
		return nil, &daemonSenderError{}
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		a8Util.Log.Fatal("Failed to read response from daemon")
	}

	if err := proto.Unmarshal(body, a8Response); err != nil {
		return nil, &daemonSenderError{}
	}

	errMsg, ok := a8Response.Response.(*pb.A8Response_Error)
	if ok {
		return nil, fmt.Errorf(errMsg.Error.Message)
	}

	return a8Response, nil
}

func daemonTransaction() (*pb.Transaction, error) {
	transaction := &pb.Transaction{}

	var fullURL = fmt.Sprintf("%s/transaction", Config.URL)

	req, err := http.NewRequest(http.MethodGet, fullURL, nil)
	if err != nil {
		return nil, err
	}

	req.Close = true
	resp, err := client.Do(req)

	if err != nil {
		return nil, &daemonSenderError{}
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		a8Util.Log.Error("Failed to read response from daemon")
		return nil, &daemonSenderError{}
	}

	if err := proto.Unmarshal(body, transaction); err != nil {
		return nil, &daemonSenderError{}
	}

	return transaction, nil
}

//should maybe be in its own file
//signRequest adds a signature to an a8 request, if a private key is part of the daemonClient
func signRequest(a8Req *pb.A8Request) error {
	pk := Config.PrivateKey

	//if there is no private key don't sign it
	if pk == nil {
		return nil
	}

	byteCode, err := proto.Marshal(a8Req)
	if err != nil {
		return err
	}

	signature, err := a8crypto.SignMessage(pk, byteCode)
	if err != nil {
		return err
	}

	a8Req.Signature = signature

	return nil
}
