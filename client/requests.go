package client

import (
	a8Util "gitlab.com/autom8.network/go-a8-util"
	a8crypto "gitlab.com/autom8.network/go-a8-util/crypto"
	"gitlab.com/autom8.network/go-a8-util/fqn"
	pb "gitlab.com/autom8.network/go-a8-util/proto"
)

//Invoke prepares and calls invoke via the a8 daemon
func Invoke(fullyQualifiedName string, input []byte) (*pb.InvokeResponse, error) {
	err := fqn.Validate(fullyQualifiedName)
	if err != nil {
		return nil, err
	}

	invokeRequest := &pb.A8Request{
		Request: &pb.A8Request_Invoke{
			Invoke: &pb.InvokeRequest{
				Fqn:   fullyQualifiedName,
				Input: input,
			},
		},
	}

	if err := getAndAddTransactionInformation(invokeRequest); err != nil {
		return nil, err
	}

	invokeResponse, err := daemonMessage(invokeRequest)
	if err != nil {
		return nil, err
	}

	return invokeResponse.GetInvoke(), nil
}

//NamespaceInfo finds information about a namespace, such as the details of a function, or all of a namespaces children
func NamespaceInfo(ns string) (*pb.ShowResponse, error) {
	namespaceReqeust := &pb.A8Request{
		Request: &pb.A8Request_Show{
			Show: &pb.ShowRequest{
				Ns: ns,
			},
		},
	}

	if err := getAndAddTransactionInformation(namespaceReqeust); err != nil {
		return nil, err
	}

	namespaceResponse, err := daemonMessage(namespaceReqeust)
	if err != nil {
		return nil, err
	}

	return namespaceResponse.GetShow(), nil
}

// DeployFn upserts a function
func DeployFn(FQN string, image string, timeout int32, idleTimeout int32, memory int32) (*pb.FnData, error) {
	err := fqn.Validate(FQN)
	if err != nil {
		return nil, err
	}

	namespaceReqeust := &pb.A8Request{
		Request: &pb.A8Request_Deploy{
			Deploy: &pb.FnData{
				Fqn:         FQN,
				Image:       image,
				Memory:      memory,
				Timeout:     timeout,
				IdleTimeout: idleTimeout,
			},
		},
	}

	if err := getAndAddTransactionInformation(namespaceReqeust); err != nil {
		return nil, err
	}

	deployResponse, err := daemonMessage(namespaceReqeust)
	if err != nil {
		return nil, err
	}

	return deployResponse.GetDeploy(), nil
}

func AddLogger(sysLogUrl string, appName string) error {

	namespaceReqeust := &pb.A8Request{
		Request: &pb.A8Request_Logger{
			Logger: &pb.LoggerRequest{
				SyslogUrl: sysLogUrl,
				AppName: appName,
			},
		},
	}

	if err := getAndAddTransactionInformation(namespaceReqeust); err != nil {
		return err
	}

	_, err := daemonMessage(namespaceReqeust)
	if err != nil {
		return err
	}

	return nil
}


func getAndAddTransactionInformation(request *pb.A8Request) error {
	transaction, err := daemonTransaction()
	if err != nil {
		return err
	}

	pubKeyPem := a8crypto.PublicKeyToPem(&Config.PrivateKey.PublicKey)

	transaction.PublicKey = string(pubKeyPem)
	transaction.Nonce = a8Util.A8uuid()

	request.Transaction = transaction

	return nil
}

