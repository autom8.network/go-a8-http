package client

import (
	"crypto/rsa"
	"strconv"
)

//DaemonConfig holds information about how to talk to a8 daemon over web and private key to sign requests
type DaemonConfig struct {
	Domain     string
	Port       int
	Protocol   string
	URL        string
	PrivateKey *rsa.PrivateKey
}

// NewDaemonConfig creates a new daemon config and generates the URL
func NewDaemonConfig(protocol string, domain string, port int, privateKey *rsa.PrivateKey) DaemonConfig {
	if len(protocol) == 0 {
		protocol = "http"
	}

	if len(domain) == 0 {
		domain = "localhost"
	}

	if port <= 0 || port > 65535 {
		port = 3035
	}

	return DaemonConfig{
		Protocol:   protocol,
		Domain:     domain,
		Port:       port,
		PrivateKey: privateKey,
		URL:        protocol + "://" + domain + ":" + strconv.Itoa(port) + "/",
	}
}
