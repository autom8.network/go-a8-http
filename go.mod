module gitlab.com/autom8.network/go-a8-http

go 1.12

require (
	github.com/golang/protobuf v1.3.1
	github.com/google/uuid v1.1.1
	github.com/gorilla/mux v1.7.1
	github.com/mattn/go-sqlite3 v1.10.0 // indirect
	gitlab.com/autom8.network/go-a8-util v0.0.0-20190624211909-8b2090c44ebe
)

// replace gitlab.com/autom8.network/go-a8-util => ../go-a8-util
