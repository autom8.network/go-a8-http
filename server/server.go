package server

import (
	"context"
	"github.com/gorilla/mux"
	a8util "gitlab.com/autom8.network/go-a8-util"
	pb "gitlab.com/autom8.network/go-a8-util/proto"

	"net/http"
	"strings"
)

type a8RequstProcessor func(request *pb.A8Request) (*pb.A8Response, error)

type transactionProcessor func() (*pb.Transaction, error)

//A8httpServer creates a server for handling incoming requests and sending them over the network. The Daemon uses this code to get data from CLI
type A8httpServer struct {
	port            string
	a8reqProc       a8RequstProcessor
	transactionSend transactionProcessor
	ctx             context.Context
}

//New creates a new a8HttpServer
func New(ctx context.Context, port string) *A8httpServer {
	return &A8httpServer{
		port: port,
		ctx:  ctx,
	}
}

//AddA8RequestProcessor adds a requests process to the server, this handles A8Requests and sends the information over
// a8 network
func (s *A8httpServer) AddA8RequestProcessor(reqProc a8RequstProcessor) {
	s.a8reqProc = reqProc
}

//AddTransactionsProcessor adds a transaction process to the server, this handles asking for transaction information
func (s *A8httpServer) AddTransactionsProcessor(transactionProc transactionProcessor) {
	s.transactionSend = transactionProc
}

//Start starts the server and sets up handlers
func (s *A8httpServer) Start() {
	r := mux.NewRouter()

	r.HandleFunc("/message", s.messageHandler).Methods(http.MethodPost)
	r.HandleFunc("/transaction", s.transactionHandler).Methods(http.MethodGet)

	//this is for backwards compatibility for funtions calling other functions.
	r.HandleFunc("/invoke/{fqn}", s.InvokeHandler).Methods(http.MethodPost)

	str := []string{":", s.port}
	var httpPortString = strings.Join(str, "")

	portLog := a8util.Log.WithFields(a8util.Fields{
		"port": httpPortString,
	})

	portLog.Info("Listening on port")

	portLog.Fatal(http.ListenAndServe(httpPortString, r))
}
