package server

import (
	"fmt"
	"github.com/golang/protobuf/proto"
	"github.com/gorilla/mux"
	"io/ioutil"
	"net/http"

	a8util "gitlab.com/autom8.network/go-a8-util"

	pb "gitlab.com/autom8.network/go-a8-util/proto"
)

func (hs *A8httpServer) messageHandler(w http.ResponseWriter, r *http.Request) {
	a8Request := &pb.A8Request{}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		hs.httpError("problem parsing A8Request", err, w)
		return
	}

	if err := proto.Unmarshal(body, a8Request); err != nil {
		hs.httpError("problem unmarshalling A8Request", err, w)
		return
	}

	rawBytes, err := hs.sendRequestOverA8(a8Request)
	if err != nil {
		hs.httpError("problem sending a8request", err, w)
		return
	}

	_, err = w.Write(rawBytes)

	if err != nil {
		hs.httpError("problem sending A8response", err, w)
		return
	}
}

func (hs *A8httpServer) transactionHandler(w http.ResponseWriter, r *http.Request) {
	transactionData, err := hs.transactionSend()
	if err != nil {
		return
	}

	rawBytes, err := proto.Marshal(transactionData)
	if err != nil {
		hs.httpError("problem marshalling transactionData", err, w)
		return
	}

	_, err = w.Write(rawBytes)

	if err != nil {
		hs.httpError("problem sending transactionData", err, w)
		return
	}
}

// InvokeHandler handles the /invoke/ path, for backwards compatibility of functions when called by other fuctnions
func (hs *A8httpServer) InvokeHandler(w http.ResponseWriter, r *http.Request) {
	invokeRequest, err := buildInvoke(r)
	if err != nil {
		derr := "problem building invoke command"
		a8util.Log.Error(derr, err)
		http.Error(w, derr, 500)
		return
	}

	a8Request := &pb.A8Request{
		Request: invokeRequest,
	}

	a8Response, err := hs.a8reqProc(a8Request)
	if err != nil {
		hs.httpError("problem sending over a8", err, w)
		return
	}

	a8util.Log.WithFields(a8util.Fields{
		"a8Response" : a8util.TruncateString(fmt.Sprintf("%s",a8Response), 1000),
	}).Info("a8Request response returned")

	rawBytes := a8Response.GetInvoke().Data

	_, err = w.Write(rawBytes)

	if err != nil {
		hs.httpError("problem sending A8response", err, w)
		return
	}
}

//httpError is a helper function to create and send an http error
func (hs *A8httpServer) httpError(msg string, err error, w http.ResponseWriter) {
	derr := msg
	a8util.Log.Error(derr, err)
	http.Error(w, derr, 500)
}

func buildInvoke(r *http.Request) (*pb.A8Request_Invoke, error) {
	vars := mux.Vars(r)
	fqn := vars["fqn"]

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, err
	}

	if body == nil {
		body = []byte("{}")
	}

	a8util.Log.WithFields(a8util.Fields{
		"FQN": fqn,
	}).Info("in invoke bulider deconstructing")

	return &pb.A8Request_Invoke{
		Invoke: &pb.InvokeRequest{
			Fqn:   fqn,
			Input: body,
		},
	}, nil
}

func (hs *A8httpServer) sendRequestOverA8(a8Req *pb.A8Request) ([]byte, error) {
	//this is where the data is actually send over libp2p and processed
	a8Response, err := hs.a8reqProc(a8Req)
	if err != nil {
		return nil, fmt.Errorf("problem sending A8response over libp2p network %s", err)
	}

	return proto.Marshal(a8Response)
}
